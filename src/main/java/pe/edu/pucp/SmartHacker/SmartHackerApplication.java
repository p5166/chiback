package pe.edu.pucp.SmartHacker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartHackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartHackerApplication.class, args);
	}

}
